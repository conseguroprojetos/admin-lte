nativa
        .controller('PartidasCtrl2',
                ['$scope', '$filter','Partidas2', 'PartidaGol', '$state', 'PartidaParticipantes', 'PartidaResumo','$stateParams',
                    function ($scope,$filter, Partidas, PartidaGol, $state, PartidaParticipantes, PartidaResumo, $stateParams) {


                        var calcularArtilheira = function(resumo) {
                            var orderBy = $filter('orderBy');
                            $scope.order = function(predicate, reverse){
                                resumo = orderBy(resumo, predicate, reverse);
                            }
                            $scope.order('-gols',false);
                            return resumo;
                        }
                        
                        var calcularAssistente = function(resumo) {
                            var orderBy = $filter('orderBy');
                            $scope.order = function(predicate, reverse){
                                resumo = orderBy(resumo, predicate, reverse);
                            }
                            $scope.order('-assistencias',false);
                            return resumo;
                        }

                        $scope.registros = [];
                        $scope.load = function () {
                            $scope.registros = Partidas.get(function (response) {
                                $scope.registros = response._embedded.partida;
                            });
                        }

                        $scope.add = function (partida) {
                            Partidas.save(partida, function (value, responseHeaders) {
                                $state.go('app.partidas');
                            }, function (httpResponse) {
                                console.log('Deu erro', arguments);
                            });
                        };

                        $scope.obterAtletas = function () {
                            Partidas.get({partida: $stateParams.id_partida}, function (response) {
                                $scope.atletas = response._embedded.partida;
                            });
                        }
                        
                        $scope.resumoPartida = function () {
                            PartidaResumo.get({partida: $stateParams.id_partida}, function (response) {
                                var resumo = response._embedded.partida_resumo;
                                
                                // Calcula o Total baseado na fórmula
                                angular.forEach(resumo, function(atleta, key) {
                                    resumo[key].total = (atleta.gols*1) + (atleta.assistencias*2) + 1;
                                });
                                
                                $scope.registros = resumo;
                                $scope.artilheira = calcularArtilheira(resumo);
                                $scope.assistente = calcularAssistente(resumo);
                            });
                        }



                        $scope.addGolByPartida = function (id_atleta) {
                            // @todo
                            console.log($stateParams);
                            $scope.golPartida =
                                    {
                                        "gol": $stateParams.id_atleta,
                                        "assistencia": id_atleta,
                                        "partida": $stateParams.id_partida
                                    };
                            console.log($scope.golPartida);

                            PartidaGol.save($scope.golPartida, function (value, responseHeaders) {
                                $state.go('app.partida_iniciada', {id_partida: $stateParams.id_partida});
                            }, function (httpResponse) {
                                console.log('Deu erro', arguments);
                            });

                        }

                    }
                ]
                )

        ;
