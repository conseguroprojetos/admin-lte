nativa
        .controller('AtletasCtrl2',
                ['$scope','Atletas2','PartidaParticipantes','$state','$stateParams',
                    function($scope,Atletas,PartidaParticipantes,$state,$stateParams){
                        $scope.registros = [];
                        $scope.load = function(){
                            $scope.registros = Atletas.get(function(response) {
                                $scope.registros = response._embedded.atleta;
                            });
                        }
                        
                        $scope.add = function(atleta){
                            Atletas.save(atleta, function(value, responseHeaders) {
                                $state.go('app.atletas');
                            }, function(httpResponse) {
                                console.log('Deu erro', arguments);
                            });
                        };
                        
                        $scope.obter = function(){
                            Atletas.get({id: $stateParams.id_atleta},function(response) {
                                $scope.atleta = response;
                            });
                        }
                        
                        $scope.addAtletaByPartida = function (id_atleta) {
                            // @todo
                            $scope.atletaPartida =
                                    {
                                        "atleta": id_atleta,
                                        "partida": $stateParams.id_partida
                                    };
                            PartidaParticipantes.save($scope.atletaPartida, function (value, responseHeaders) {
                               $state.go('app.partida_iniciada',{id_partida:$stateParams.id_partida});
                            }, function (httpResponse) {
                                console.log('Deu erro', arguments);
                            });

                        }
                    }
                ]
)

;
